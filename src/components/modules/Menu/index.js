import React from 'react';
import menu from './menu.json';
import List from './List';

const Menu = ()=>{
    return (
            <ul>
               <List
                menu={menu}
               />
            </ul>
    );
}

export default Menu;